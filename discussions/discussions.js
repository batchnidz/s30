// MonggoDB aggregation Query Case studies

db.fruits.insertMany([
{
	name : "Apple",
	color : "Red",
	stock : 20,
	price: 40,
	supplier_id : 1,
	onSale : true,
	origin: [ "Philippines", "US" ]
},

{
	name : "Banana",
	color : "Yellow",
	stock : 15,
	price: 20,
	supplier_id : 2,
	onSale : true,
	origin: [ "Philippines", "Ecuador" ]
},

{
	name : "Kiwi",
	color : "Green",
	stock : 25,
	price: 50,
	supplier_id : 1,
	onSale : true,
	origin: [ "US", "China" ]
},

{
	name : "Mango",
	color : "Yellow",
	stock : 10,
	price: 120,
	supplier_id : 2,
	onSale : false,
	origin: [ "Philippines", "India" ]
}     	
]);


//  Aggregation Pipeline

/*
	Documentation on aggregation
	- https://www.mongodb.com/docs/manual/reference/operator/aggregation-pipeline/


	1st Phase - $match phase is responsible for gathering the initial items base on a certain argument/criteria

	2nd Phase - $group phase is responsible for grouping the specific fields from the docs after the criteria has been determined.

	3rd Phase - Optional - $project phase is responsible for excluding certain fields that do not need to show up in the final result.

*/

// stage only
// .count() - is for counting an object or specific elements or fields.

db.fruits.count();

// the $count stage returns a count of the remaining docs in the aggregation pipeline and assigns the value to a field.

db.fruits.aggregate([
		{$count: "fruits"}
	]);


//  with $match

db.fruits.aggregate([
        {
            $match: {onSale: true }
        },
		{$count: "Fruit on Sale"}
	]);


// $match and $group

db.fruits.aggregate([
		{
			$match: {onSale: true }
		},
		{
			$group: {
				_id: "$supplier_id",
				totalStocks: {$sum: "$stock"}
				
		}
	]);

// plus one

db.fruits.aggregate([
		{
			$match: {onSale: "true" }
		},
		{
			$group: {
				_id: "$supplier_id",
				totalStocks: {$sum: "$stock"},
                totalPrice: {$sum: "$price"}
			}
		}
	]);


// 3rd phase

db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: {
				_id: "$supplier_id",
				totalStocks: {$sum: "$stock"},
                totalPrice: {$sum: "$price"}
			}
		},
		{
			$project: {totalStocks: 1}
		}
	]);


// 

db.fruits.aggregate ([
	{
		$match: { onSale: true } 
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: { $sum: "$stock"},
                        price: { $sum: "$price"}
		}
	},
	{
		$project: { totalStocks: 1, _id:0 }
	}

])


// $sort operator ascending (1) or descending (-1)

db.fruits.aggregate ([
	{
		$match: { onSale: true } 
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: { $sum: "$stock"},
                        price: { $sum: "$price"}
		}
	},
	{
		$project: { totalStocks: 1, _id:0 }
	},
        {
            $sort: { price: -1}
            }

])


// $unwind operator - responsible for deconstructing an array and using them as the unique identifiers for each row in the result.

db.fruits.aggregate([
	{$unwind : "$origin"}
]);


db.fruits.aggregate([
	{$unwind : "$origin"},
        {$project : {origin:1, name: 1}}
]);


db.fruits.aggregate([
	{$unwind : "$origin"},
        {$project : {origin:1, name: 1, onSale: 1}},
        {$match: {onSale:false}}
]);


/*
// For Activity
$avg, $min, $max


https://www.mongodb.com/docs/manual/reference/operator/aggregation/group/#mongodb-pipeline-pipe.-group


*/


// AVG

db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: {
				_id: "$supplier_id",
				totalStocks: {$avg: "$stock"},
                totalPrice: {$avg: "$price"}
			}
		},
		{
			$project: {totalStocks: 1}
		}
	]);
